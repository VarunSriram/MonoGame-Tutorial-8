﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RotatingObjects
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private DebugSprite arrow1, arrow2;
        private Color clearColor, collisionColor;
        private readonly Rectangle gameDimensions;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 720
            };
            
            Content.RootDirectory = "Content";
            gameDimensions = new Rectangle(0, 0, 1280, 720);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            arrow1 = new DebugSprite(new Vector2(0, (graphics.GraphicsDevice.Viewport.Height / 2.0f - 26)), Color.White,50,0,MathHelper.ToRadians(-5),3.2f,gameDimensions);
            arrow2 = new DebugSprite(new Vector2(graphics.GraphicsDevice.Viewport.Width, 
                (graphics.GraphicsDevice.Viewport.Height / 2.0f -26)), Color.Green, 60,MathHelper.Pi,MathHelper.ToRadians(16),0.25f,gameDimensions);

            clearColor = Color.ForestGreen;
            collisionColor = Color.Red;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            arrow1.loadContent(Content, GraphicsDevice, "arrow");
            arrow2.loadContent(Content, GraphicsDevice, "arrow");


        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            arrow1.Unload();
            arrow2.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            arrow1.Update(gameTime);
            arrow2.Update(gameTime);

            arrow1.Collision(arrow2);




            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            if(arrow1.Collided || arrow2.Collided)
            {
                GraphicsDevice.Clear(collisionColor);
            }
            else
            {
                GraphicsDevice.Clear(clearColor);
            }

            spriteBatch.Begin();
            arrow1.Draw(spriteBatch, gameTime);
            arrow2.Draw(spriteBatch, gameTime);
            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
