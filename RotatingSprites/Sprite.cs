﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotatingObjects
{
    public class Sprite
    {
        private Color color;
        private Vector2 velocity;
        private float angle;
        private float rotationSpeed;
        private Vector2 scale;
        private Vector2 position;
        private Rectangle rectangle;
        private Texture2D texture;
        private Rectangle? bounds;
        private Vector2 origin;

        public Sprite(Vector2 pos, float speed = 0, float angle = 0,float rotationSpeed = 0, float scale = 1.0f, Rectangle? bounds = null)
        {
           
            this.position = pos;
            this.angle = angle;
            this.rotationSpeed = rotationSpeed;
            this.scale = new Vector2(scale,scale);

            this.velocity = new Vector2((float)(speed * Math.Cos(angle)), (float)(speed * Math.Sin(angle)));

            this.texture = null;
            this.color = Color.White;
            this.origin = Vector2.Zero;

            this.bounds = bounds;
        }

        protected Texture2D Texture => texture;

        public Vector2 Position => position;

        public Rectangle Rectange => rectangle;

        public Vector2 Origin => origin;

        public bool Collided { get; private set; }

        public void loadContent(ContentManager content, GraphicsDevice graphicDevice, string assetName)
        {
            texture = content.Load<Texture2D>(assetName);
            OnContentLoaded(content, graphicDevice);
        }

        //Method to do something once Content is loaded.
        protected virtual void OnContentLoaded(ContentManager content, GraphicsDevice graphicsDevice)
        {
            origin = new Vector2(texture.Width / 2f, texture.Height / 2f);

            UpdateRectangle();
            
        }

        

        private void UpdateRectangle()
        {
            Vector2 topLeft = position - origin;

            rectangle = new Rectangle((int)topLeft.X, (int)topLeft.Y,texture.Width, texture.Height);

        }

        public virtual void Unload()
        {
            texture.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            position += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

            UpdateRotation(gameTime);
            UpdateRectangle();
            CheckBounds();
        }

        private void UpdateRotation(GameTime gameTime)
        {
            angle += (float)(rotationSpeed * gameTime.ElapsedGameTime.TotalSeconds);

            if(angle < 0)
            {
                angle = MathHelper.TwoPi - Math.Abs(angle);
            }

            else if(angle > MathHelper.TwoPi)
            {
                angle = angle - MathHelper.TwoPi;
            }
        }

        private void CheckBounds()
        {
            if (bounds == null) return;

            Vector2 change = Vector2.Zero;
            if (rectangle.Left <= bounds.Value.X)
            {
                change.X = bounds.Value.X - rectangle.Left;
            }

            else if (rectangle.Right >= bounds.Value.Right)
            {
                change.X = bounds.Value.Right - rectangle.Right;
            }

            if (rectangle.Top <= bounds.Value.Y)
            {
                change.Y = bounds.Value.Y - rectangle.Top;
            }

            else if (rectangle.Bottom >= bounds.Value.Bottom)
            {
                change.Y = bounds.Value.Bottom - rectangle.Bottom;
            }

            if (change == Vector2.Zero) return;

            position = new Vector2((int)position.X + change.X, (int)position.Y + change.Y);
            UpdateRectangle();

        }

        public bool Collision(Sprite target)
        {
            bool intersects = rectangle.Intersects(target.rectangle) && PerPixelCollision(target);
            Collided = intersects;
            target.Collided = intersects;
            return intersects;

        }

        private bool PerPixelCollision(Sprite target)
        {
            var sourceColors = new Color[texture.Width * texture.Height];
            texture.GetData(sourceColors);
            var targetColors = new Color[target.texture.Width * target.texture.Height];
            target.texture.GetData(targetColors);

            var left = Math.Max(rectangle.Left, target.rectangle.Left);
            var top = Math.Max(rectangle.Top, target.rectangle.Top);
            var width = Math.Min(rectangle.Right, target.rectangle.Right) - left;
            var height = Math.Min(rectangle.Bottom, target.rectangle.Bottom) - top;
            var intersectingRectangle = new Rectangle(left, top, width, height);

            for( var x = intersectingRectangle.Left; x <  intersectingRectangle.Right; x++)
            {
                for(var y = intersectingRectangle.Top; y < intersectingRectangle.Bottom; y++)
                {
                    var sourceColor = sourceColors[(x - rectangle.Left) + (y - rectangle.Top) * texture.Width];
                    var targetColor = targetColors[(x - target.rectangle.Left) + (y - target.rectangle.Top) * target.texture.Width];
                    
                    //Return true if the source and target pixel are not transparent.
                    if (sourceColor.A > 0 && targetColor.A > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        //Assume Begin was already called when using this mehtod
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(texture, position, null, null, origin, angle, scale, color);
        }
    }
}
